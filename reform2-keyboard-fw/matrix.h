/*
  MNT Reform 2.0 Keyboard Firmware
  See keyboard.c for Copyright
  SPDX-License-Identifier: MIT
*/

#include "keyboard.h"

// Every line of `matrix` is a row of the keyboard, starting from the top.
// Check keyboard.h for the definitions of the default rows.
uint16_t matrix[KBD_MATRIX_SZ] = {
  MATRIX_DEFAULT_ROW_1,
  MATRIX_DEFAULT_ROW_2,
  MATRIX_DEFAULT_ROW_3,
  MATRIX_DEFAULT_ROW_4,
  MATRIX_ROW_5,

  // Marker for layout editor (FIXME)
  KBD_EDITOR_MARKER
};

// When holding down LOWER
uint16_t matrix_lower[KBD_MATRIX_SZ] = {
  MATRIX_DEFAULT_ROW_1,
  MATRIX_LOWER_ROW_2,
  MATRIX_LOWER_ROW_3,
  MATRIX_DEFAULT_ROW_4,
  MATRIX_ROW_5
};

// When holding down RAISE
uint16_t matrix_raise[KBD_MATRIX_SZ] = {
  MATRIX_RAISE_ROW_1,
  MATRIX_RAISE_ROW_2,
  MATRIX_RAISE_ROW_3,
  MATRIX_RAISE_ROW_4,
  MATRIX_ROW_5
};
