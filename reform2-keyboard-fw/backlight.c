/*
  MNT Reform 2.0 Keyboard Firmware
  See keyboard.c for Copyright
  SPDX-License-Identifier: MIT
*/

#include <avr/io.h>
#include <stdint.h>
#include "backlight.h"

int16_t pwmval = 0;

void kbd_brightness_init(void) {
}

void kbd_brightness_inc(void) {
}

void kbd_brightness_dec(void) {
}

void kbd_brightness_set(int brite) {
}
