# Note: pass --force for first-time flash. dfu-programmer will normally refuse
# to erase a fresh chip, but you need to erase it anyway in order to clear the
# write protection fuses.
dfu-programmer atmega32u4 erase --suppress-bootloader-mem
dfu-programmer atmega32u4 flash ./keyboard.hex --suppress-bootloader-mem
dfu-programmer atmega32u4 start
