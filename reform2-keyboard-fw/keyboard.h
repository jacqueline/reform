/*
  MNT Reform 2.0 Keyboard Firmware
  See keyboard.c for Copyright
  SPDX-License-Identifier: MIT
*/

#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <string.h>

#include "Config/LUFAConfig.h"
#include "descriptors.h"
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>

// some GPIO macros
#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

// Reuse existing key codes
#define HID_KEYBOARD_SC_MEDIA_BRIGHTNESS_DOWN HID_KEYBOARD_SC_MEDIA_RELOAD
#define HID_KEYBOARD_SC_MEDIA_BRIGHTNESS_UP HID_KEYBOARD_SC_MEDIA_CALCULATOR

// Top row, left to right
#define MATRIX_DEFAULT_ROW_1 \
  KEY_ESCAPE,\
  KEY_1,\
  KEY_2,\
  KEY_3,\
  KEY_4,\
  KEY_5,\
  0x00,\
  0x00,\
  KEY_6,\
  KEY_7,\
  KEY_8,\
  KEY_9,\
  KEY_0,\
  KEY_GRAVE

// Second row
#define MATRIX_DEFAULT_ROW_2 \
  KEY_TAB,\
  KEY_Q,\
  KEY_W,\
  KEY_E,\
  KEY_R,\
  KEY_T,\
  0x00,\
  0x00,\
  KEY_Y,\
  KEY_U,\
  KEY_I,\
  KEY_O,\
  KEY_P,\
  KEY_BACKSPACE

// Third row
#define MATRIX_DEFAULT_ROW_3 \
  HID_KEYBOARD_SC_LEFT_CONTROL,\
  KEY_A,\
  KEY_S,\
  KEY_D,\
  KEY_F,\
  KEY_G,\
  0x00,\
  0x00,\
  KEY_H,\
  KEY_J,\
  KEY_K,\
  KEY_L,\
  KEY_SEMICOLON,\
  KEY_QUOTE

// Fourth row
#define MATRIX_DEFAULT_ROW_4 \
  HID_KEYBOARD_SC_LEFT_SHIFT,\
  KEY_Z,\
  KEY_X,\
  KEY_C,\
  KEY_V,\
  KEY_B,\
  KEY_LBRACK,\
  KEY_RBRACK,\
  KEY_N,\
  KEY_M,\
  KEY_COMMA,\
  KEY_DOT,\
  KEY_SLASH,\
  HID_KEYBOARD_SC_RIGHT_SHIFT

// Fifth row
#define MATRIX_ROW_5 \
  0x00,\
  0x00,\
  0x00,\
  HID_KEYBOARD_SC_LEFT_ALT,\
  HID_KEYBOARD_SC_LEFT_GUI,\
  KEY_LOWER,\
  KEY_SPACE,\
  KEY_ENTER,\
  KEY_RAISE,\
  HID_KEYBOARD_SC_RIGHT_ALT,\
  KEY_CIRCLE,\
  0x00,\
  0x00,\
  0x00

#define MATRIX_RAISE_ROW_1 \
  KEY_F1,\
  KEY_F2,\
  KEY_F3,\
  KEY_F4,\
  KEY_F5,\
  KEY_F6,\
  0x00,\
  0x00,\
  KEY_F7,\
  KEY_F8,\
  KEY_F9,\
  KEY_F10,\
  KEY_F11,\
  KEY_F12

#define MATRIX_RAISE_ROW_2 \
  KEY_ESCAPE, KEY_EXCLAIM, KEY_AT, KEY_HASH, KEY_DOLLAR, KEY_PERCENT, 0x00,\
  0x00, KEY_CARET, KEY_AMP, KEY_AST, KEY_LPAREN, KEY_RPAREN, KEY_BACKSPACE

#define MATRIX_RAISE_ROW_3 \
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  KEY_MINUS,\
  KEY_EQUAL,\
  KEY_LBRC,\
  KEY_RBRC,\
  KEY_PIPE,\
  KEY_GRAVE

#define MATRIX_RAISE_ROW_4 \
  HID_KEYBOARD_SC_LEFT_SHIFT,\
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,\
  KEY_UNDERSCORE, KEY_PLUS, KEY_LBRACK, KEY_RBRACK, KEY_BACKSLASH, KEY_TILDE

#define MATRIX_LOWER_ROW_2 \
  KEY_ESCAPE,\
  KEY_1,\
  KEY_2,\
  KEY_3,\
  KEY_4,\
  KEY_5,\
  0x00,\
  0x00,\
  KEY_6,\
  KEY_7,\
  KEY_8,\
  KEY_9,\
  KEY_0,\
  KEY_BACKSPACE

#define MATRIX_LOWER_ROW_3 \
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  0x00,\
  HID_KEYBOARD_SC_LEFT_ARROW,\
  HID_KEYBOARD_SC_DOWN_ARROW,\
  HID_KEYBOARD_SC_UP_ARROW,\
  HID_KEYBOARD_SC_RIGHT_ARROW,\
  0x00,\
  0x00

// MediaController USB Report
typedef struct
{
  unsigned BrightnessDown : 1;
  unsigned BrightnessUp   : 1;
  unsigned PreviousTrack  : 1;
  unsigned PlayPause      : 1;
  unsigned NextTrack      : 1;
  unsigned Mute           : 1;
  unsigned VolumeDown     : 1;
  unsigned VolumeUp       : 1;
} ATTR_PACKED USB_MediaReport_Data_t;

void setup_hardware(void);
void reset_keyboard_state(void);

// LUFA USB handlers
void EVENT_USB_Device_Connect(void);
void EVENT_USB_Device_Disconnect(void);
void EVENT_USB_Device_ConfigurationChanged(void);
void EVENT_USB_Device_ControlRequest(void);
void EVENT_USB_Device_StartOfFrame(void);

bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize);

void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize);

#endif
