#include <avr/pgmspace.h>

static const unsigned char PROGMEM font[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x5F, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x03, 0x00, 0x03, 0x00, 0x00, 0x00,
    0x1A, 0x0E, 0x1B, 0x0E, 0x0B, 0x00,
    0x26, 0x49, 0xFF, 0x49, 0x32, 0x00,
    0x06, 0x69, 0x1F, 0x3D, 0x4B, 0x30,
    0x30, 0x4A, 0x45, 0x4A, 0x30, 0x48,
    0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x3C, 0x42, 0x81, 0x00, 0x00, 0x00,
    0x81, 0x42, 0x3C, 0x00, 0x00, 0x00,
    0x12, 0x0C, 0x07, 0x0C, 0x12, 0x00,
    0x08, 0x08, 0x3E, 0x08, 0x08, 0x00,
    0x80, 0x60, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x08, 0x08, 0x08, 0x00, 0x00,
    0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xC0, 0x30, 0x0C, 0x03, 0x00, 0x00,
    0x3E, 0x51, 0x49, 0x45, 0x3E, 0x00,
    0x02, 0x7F, 0x00, 0x00, 0x00, 0x00,
    0x42, 0x61, 0x51, 0x49, 0x46, 0x00,
    0x21, 0x49, 0x4D, 0x4B, 0x31, 0x00,
    0x18, 0x14, 0x12, 0x7F, 0x10, 0x00,
    0x27, 0x45, 0x45, 0x45, 0x39, 0x00,
    0x3C, 0x4A, 0x49, 0x49, 0x30, 0x00,
    0x01, 0x01, 0x71, 0x0D, 0x03, 0x00,
    0x36, 0x49, 0x49, 0x49, 0x36, 0x00,
    0x06, 0x49, 0x49, 0x29, 0x1E, 0x00,
    0x44, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x80, 0x64, 0x00, 0x00, 0x00, 0x00,
    0x08, 0x14, 0x22, 0x00, 0x00, 0x00,
    0x14, 0x14, 0x14, 0x14, 0x14, 0x00,
    0x22, 0x14, 0x08, 0x00, 0x00, 0x00,
    0x02, 0x51, 0x09, 0x06, 0x00, 0x00,
    0x3C, 0x5A, 0xA5, 0xBD, 0x22, 0x1C,
    0x70, 0x1C, 0x13, 0x1C, 0x70, 0x00,
    0x7F, 0x49, 0x49, 0x49, 0x36, 0x00,
    0x3E, 0x41, 0x41, 0x41, 0x22, 0x00,
    0x7F, 0x41, 0x41, 0x22, 0x1C, 0x00,
    0x7F, 0x49, 0x49, 0x41, 0x00, 0x00,
    0x7F, 0x09, 0x09, 0x01, 0x00, 0x00,
    0x3E, 0x41, 0x41, 0x49, 0x3A, 0x00,
    0x7F, 0x08, 0x08, 0x08, 0x7F, 0x00,
    0x7F, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x30, 0x40, 0x40, 0x40, 0x3F, 0x00,
    0x7F, 0x08, 0x14, 0x22, 0x41, 0x00,
    0x7F, 0x40, 0x40, 0x40, 0x00, 0x00,
    0x7F, 0x02, 0x04, 0x02, 0x7F, 0x00,
    0x7F, 0x03, 0x0C, 0x30, 0x7F, 0x00,
    0x3E, 0x41, 0x41, 0x41, 0x3E, 0x00,
    0x7F, 0x09, 0x09, 0x09, 0x06, 0x00,
    0x3E, 0x41, 0x61, 0xC1, 0x3E, 0x00,
    0x7F, 0x09, 0x19, 0x29, 0x46, 0x00,
    0x26, 0x49, 0x49, 0x49, 0x32, 0x00,
    0x01, 0x01, 0x7F, 0x01, 0x01, 0x00,
    0x3F, 0x40, 0x40, 0x40, 0x3F, 0x00,
    0x07, 0x18, 0x60, 0x18, 0x07, 0x00,
    0x0F, 0x70, 0x0C, 0x70, 0x0F, 0x00,
    0x63, 0x14, 0x08, 0x14, 0x63, 0x00,
    0x03, 0x04, 0x78, 0x04, 0x03, 0x00,
    0x71, 0x49, 0x45, 0x43, 0x00, 0x00,
    0xFF, 0x81, 0x00, 0x00, 0x00, 0x00,
    0x03, 0x0C, 0x30, 0xC0, 0x00, 0x00,
    0x81, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0x02, 0x01, 0x02, 0x00, 0x00, 0x00,
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x48, 0x7E, 0x49, 0x41, 0x20, 0x00,
    0x20, 0x54, 0x54, 0x78, 0x00, 0x00,
    0x7F, 0x44, 0x44, 0x38, 0x00, 0x00,
    0x38, 0x44, 0x44, 0x28, 0x00, 0x00,
    0x38, 0x44, 0x44, 0x7F, 0x00, 0x00,
    0x38, 0x54, 0x54, 0x18, 0x00, 0x00,
    0x04, 0x7E, 0x05, 0x01, 0x00, 0x00,
    0x18, 0xA4, 0xA4, 0x7C, 0x00, 0x00,
    0x7F, 0x04, 0x04, 0x78, 0x00, 0x00,
    0x7D, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x80, 0x80, 0x7D, 0x00, 0x00, 0x00,
    0x7F, 0x10, 0x28, 0x44, 0x00, 0x00,
    0x3F, 0x40, 0x00, 0x00, 0x00, 0x00,
    0x7C, 0x04, 0x78, 0x04, 0x78, 0x00,
    0x7C, 0x04, 0x04, 0x78, 0x00, 0x00,
    0x38, 0x44, 0x44, 0x38, 0x00, 0x00,
    0xFC, 0x24, 0x24, 0x18, 0x00, 0x00,
    0x18, 0x24, 0x24, 0xFC, 0x00, 0x00,
    0x7C, 0x08, 0x04, 0x04, 0x00, 0x00,
    0x48, 0x54, 0x54, 0x24, 0x00, 0x00,
    0x04, 0x3F, 0x44, 0x00, 0x00, 0x00,
    0x3C, 0x40, 0x40, 0x3C, 0x00, 0x00,
    0x04, 0x18, 0x60, 0x18, 0x04, 0x00,
    0x1C, 0x60, 0x10, 0x60, 0x1C, 0x00,
    0x44, 0x28, 0x10, 0x28, 0x44, 0x00,
    0x84, 0x98, 0x60, 0x18, 0x04, 0x00,
    0x64, 0x54, 0x4C, 0x44, 0x00, 0x00,
    0x08, 0x76, 0x81, 0x00, 0x00, 0x00,
    0xE7, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x81, 0x76, 0x08, 0x00, 0x00, 0x00,
    0x02, 0x01, 0x03, 0x02, 0x01, 0x00,
    0x3C, 0x5A, 0xA5, 0xA5, 0x42, 0x3C,
    0x3C, 0x7E, 0x42, 0x42, 0x42, 0x42,
    0x42, 0x42, 0x42, 0x42, 0x42, 0x7E,
    0x3C, 0x7E, 0x7E, 0x7E, 0x7E, 0x42,
    0x42, 0x42, 0x42, 0x42, 0x42, 0x7E,
    0x3C, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E,
    0x7E, 0x42, 0x42, 0x42, 0x42, 0x7E,
    0x3C, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E,
    0x7E, 0x7E, 0x7E, 0x42, 0x42, 0x7E,
    0x3C, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E,
    0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E,
    0x3C, 0x7E, 0x42, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x42, 0x7E,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x80, 0x40, 0x40, 0x20, 0x20, 0x20,
    0x20, 0x10, 0x10, 0x10, 0x10, 0x08,
    0x04, 0x02, 0x01, 0x01, 0x02, 0x0C,
    0x30, 0x40, 0x80, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x80, 0x40, 0x40, 0x20, 0x20, 0x20,
    0x20, 0x10, 0x10, 0x10, 0x10, 0x08,
    0x04, 0x02, 0x01, 0x01, 0x02, 0x0C,
    0x30, 0x40, 0x80, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xF8, 0xF8,
    0xF8, 0xF8, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x80, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x1E, 0xE1, 0x00,
    0x00, 0x01, 0x01, 0x02, 0x02, 0x01,
    0x00, 0x00, 0x00, 0x00, 0x80, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x80, 0x00, 0x30,
    0x30, 0x00, 0x00, 0x01, 0xE1, 0x1A,
    0x06, 0x09, 0x31, 0x35, 0x01, 0x8A,
    0x7C, 0x00, 0x00, 0x80, 0x80, 0x80,
    0x80, 0x40, 0x40, 0x40, 0x40, 0x20,
    0x20, 0x20, 0x20, 0x10, 0x10, 0x10,
    0x10, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x04, 0x04, 0x04, 0x04, 0x04, 0x02,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x1E, 0xE1, 0x00,
    0x00, 0x01, 0x01, 0x02, 0x02, 0x81,
    0x80, 0x80, 0x00, 0x00, 0x80, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x80, 0x00, 0x30,
    0x30, 0x00, 0x00, 0x01, 0x01, 0x02,
    0x04, 0x08, 0x10, 0x20, 0x43, 0x87,
    0x07, 0x01, 0x00, 0xB8, 0xBC, 0xBE,
    0x9F, 0x5F, 0x5F, 0x4F, 0x4C, 0x20,
    0x20, 0x20, 0x20, 0x10, 0x10, 0x10,
    0x10, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x04, 0x04, 0x04, 0x04, 0x04, 0x02,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x80, 0x70, 0x0C, 0x03,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x01,
    0x00, 0x40, 0xA0, 0x21, 0x22, 0x12,
    0x11, 0x11, 0x11, 0x09, 0x08, 0x08,
    0x08, 0x08, 0x04, 0x04, 0x04, 0x04,
    0x04, 0x04, 0x02, 0x02, 0x02, 0x01,
    0x01, 0x01, 0x01, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x80, 0x70, 0x0C, 0x03,
    0x00, 0x00, 0x18, 0x06, 0x05, 0x98,
    0x99, 0x84, 0x43, 0x7C, 0x41, 0x41,
    0x40, 0x40, 0x20, 0x21, 0x22, 0x12,
    0x11, 0x11, 0x11, 0x09, 0x08, 0x08,
    0x08, 0x08, 0x04, 0x04, 0x08, 0x08,
    0x10, 0x10, 0x10, 0x10, 0x10, 0x11,
    0x0F, 0x01, 0x3D, 0x7C, 0xFC, 0xFC,
    0xFC, 0xFC, 0xFC, 0x3C, 0x0C, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x40, 0x40, 0x40, 0x40, 0x40, 0x20,
    0x20, 0x20, 0x20, 0x20, 0x10, 0x10,
    0x10, 0x10, 0x10, 0x08, 0x08, 0x08,
    0x08, 0x08, 0x04, 0x04, 0x04, 0x04,
    0x04, 0x02, 0x03, 0x7A, 0x7A, 0x79,
    0x79, 0x79, 0x79, 0x39, 0x31, 0x02,
    0x02, 0x04, 0x04, 0x08, 0x08, 0x08,
    0x08, 0x08, 0x07, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x40, 0x40, 0x40, 0x40, 0x40, 0x20,
    0x20, 0x20, 0x20, 0x20, 0x10, 0x10,
    0x10, 0x10, 0x10, 0x08, 0x08, 0x08,
    0x08, 0x08, 0x04, 0x04, 0x04, 0x04,
    0x04, 0x02, 0x03, 0x02, 0x02, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
    0x03, 0x03, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

