/*
  MNT Reform 2.0 Keyboard Firmware
  See keyboard.c for Copyright
  SPDX-License-Identifier: MIT
*/

#include <avr/io.h>
#include <avr/sleep.h>
#include "powersave.h"
#include "keyboard.h"
#include "oled.h"
#include "menu.h"

/* Setup the AVR to enter the Power-Down state to greatly save power.
 * Configures all outputs to be in the low state if possible, and disables
 * services like USB and Serial.
 *
 * Will leave the ports setup so that the Circle key row is being scanned
 * so when the watchdog wakes up it can quickly check and go back to sleep if not
 * Added by Chartreuse - 2021/08/14
 *
 */
void keyboard_power_off(void)
{
  USB_Disable(); // Stop USB stack so it doesn't wake us up

  // Turn off OLED to save power
  gfx_clear_screen();
  gfx_off();
  // Disable ADC to save even more power
  ADCSRA=0;

  // Set all ports not floating if possible, leaving pullups alone
  PORTB = 0b01111111;
  PORTC = 0xC0;
  PORTD = 0b10110000;
  PORTE = 0x40; // Pullup on PE6
  PORTF = 0xFF; // Pullups on PF (columns)

  // Enter Power-save mode
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_mode();

  // Zzzzzz

  // Resume and reinitialize hardware
  setup_hardware();

  // Hi!
  anim_hello();
}

