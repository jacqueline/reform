/*
  MNT Reform 2.0 Keyboard Firmware
  See keyboard.c for Copyright
  SPDX-License-Identifier: MIT
*/

// Find additional codes in lufa-master/LUFA/Drivers/USB/Class/Common/HIDClassCommon.h

#ifndef _SCANCODES_H_
#define _SCANCODES_H_

// Alphas
#define   KEY_A   0x04
#define   KEY_B   0x05
#define   KEY_C   0x06
#define   KEY_D   0x07
#define   KEY_E   0x08
#define   KEY_F   0x09
#define   KEY_G   0x0A
#define   KEY_H   0x0B
#define   KEY_I   0x0C
#define   KEY_J   0x0D
#define   KEY_K   0x0E
#define   KEY_L   0x0F
#define   KEY_M   0x10
#define   KEY_N   0x11
#define   KEY_O   0x12
#define   KEY_P   0x13
#define   KEY_Q   0x14
#define   KEY_R   0x15
#define   KEY_S   0x16
#define   KEY_T   0x17
#define   KEY_U   0x18
#define   KEY_V   0x19
#define   KEY_W   0x1A
#define   KEY_X   0x1B
#define   KEY_Y   0x1C
#define   KEY_Z   0x1D
#define   KEY_1   0x1E
#define   KEY_2   0x1F
#define   KEY_3   0x20
#define   KEY_4   0x21
#define   KEY_5   0x22
#define   KEY_6   0x23
#define   KEY_7   0x24
#define   KEY_8   0x25
#define   KEY_9   0x26
#define   KEY_0   0x27

// Specials
#define   KEY_ENTER   40
#define   KEY_ESCAPE   41
#define   KEY_BACKSPACE   42
#define   KEY_TAB   43
#define   KEY_SPACE   44
#define   KEY_MINUS 45
#define   KEY_EQUAL 46
#define   KEY_LBRACK   47
#define   KEY_RBRACK   48
#define   KEY_BACKSLASH 49
#define   KEY_SEMICOLON 51
#define   KEY_QUOTE   52
#define   KEY_GRAVE   53
#define   KEY_COMMA 54
#define   KEY_DOT   55
#define   KEY_SLASH 56

// Function keys
#define   KEY_F1   0x3A
#define   KEY_F2   0x3B
#define   KEY_F3   0x3C
#define   KEY_F4   0x3D
#define   KEY_F5   0x3E
#define   KEY_F6   0x3F
#define   KEY_F7   0x40
#define   KEY_F8   0x41
#define   KEY_F9   0x42
#define   KEY_F10   0x43
#define   KEY_F11   68
#define   KEY_F12   69
#define   KEY_F13   104
#define   KEY_F14   105
#define   KEY_F15   106
#define   KEY_F16   107
#define   KEY_F17   108
#define   KEY_F18   109
#define   KEY_F19   110
#define   KEY_F20   111

// Nav
#define   KEY_RIGHT_ARROW   0x4F
#define   KEY_LEFT_ARROW   0x50
#define   KEY_DOWN_ARROW   0x51
#define   KEY_UP_ARROW   0x52

// Special shifty keys
#define KEY_EXCLAIM 0xFF + 30
#define KEY_AT 0xFF + 31
#define KEY_HASH 0xFF + 32
#define KEY_DOLLAR 0xFF + 33
#define KEY_PERCENT 0xFF + 34
#define KEY_CARET 0xFF + 35
#define KEY_AMP 0xFF + 36
#define KEY_AST 0xFF + 37
#define KEY_LPAREN 0xFF + 38
#define KEY_RPAREN 0xFF + 39
#define KEY_UNDERSCORE 0xFF + 45
#define KEY_PLUS 0xFF + 46
#define KEY_LBRC 0xFF + 47
#define KEY_RBRC 0xFF + 48
#define KEY_PIPE 0xFF + 49
#define KEY_COLON 0xFF + 51
#define KEY_DQUOTE 0xFF + 52
#define KEY_TILDE 0xFF + 53
#define KEY_LANGLE 0xFF + 54
#define KEY_RANGLE 0xFF + 55
#define KEY_QUEST 0xFF + 56

#define   KEY_MUTE   0x7F
#define   KEY_VOLUME_UP   0x80
#define   KEY_VOLUME_DOWN   0x81
#define   KEY_BRIGHTNESS_UP 0x6F
#define   KEY_BRIGHTNESS_DOWN 0x70
#define   KEY_MEDIA_PLAY 0xE8
#define   KEY_MEDIA_PREV 0xEA
#define   KEY_MEDIA_NEXT 0xEB

#define   KEY_CIRCLE 0xA4 // "EXSEL"
#define   KEY_LOWER 0x72
#define   KEY_RAISE 0x73

#endif
