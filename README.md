# MNT Reform but with usb-pd and a cool keyboard

This is my fork of the mnt reform!

It uses usb-pd for power instead of a barrel jack, and has an unsplit lily58
keyboard layout.

This is absolutely not ready for anyone else to use, but I figure people might
be curious enough to want to poke about?

## TODO

 - actually build everything and verify that it works
 - write the configuration + flasher for the STUSB4500
 - add layers to the keyboard firmware + adapt it to my layout
 - clean up all the kicad libraries; i didn't work out how kicad deals with
   libraries until halfway through
 - probably take off all my cute gay graphics? i assume most people are Serious
   Computer Users and do not want such things
 - (stretch) modify the system controller firmware to negotiate the best profile
   at runtime, rather than relying on the flashed config
 - (stretchier) include the usb-pd profile on the system oled!
 - (stretchiest) write the kernel driver that's presumably meant to exist for
   getting power info from the LPC to the SOM
